package com.SpringBootGraph.demoSpringGraph.ServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.SpringBootGraph.demoSpringGraph.Pojo.Employees;
import com.SpringBootGraph.demoSpringGraph.Repository.EmployeeRepository;
import com.SpringBootGraph.demoSpringGraph.Service.EmployeeService;

	@Service
	public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired 
	EmployeeRepository empReposobj;

	@Override
	public Employees addEmp(Employees employees) {
		// TODO Auto-generated method stub
		empReposobj.save(employees);
		return null;
	}

	@Override
	public List<Employees> findAllEmps() {
		// TODO Auto-generated method stub
		return (List<Employees>) empReposobj.findAll();
	}

	@Override
	public Employees findSingleEmp(Object argument) {
		// TODO Auto-generated method stub
		return empReposobj.findByid(argument);
	}
	
	}
