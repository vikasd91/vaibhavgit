package com.SpringBootGraph.demoSpringGraph.Controller;

 

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SpringBootGraph.demoSpringGraph.Pojo.Employees;
import com.SpringBootGraph.demoSpringGraph.Repository.EmployeeRepository;
import com.SpringBootGraph.demoSpringGraph.Service.EmployeeService;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;




@RestController
@RequestMapping("/ivlEmployee")
public class EmployeeController{
	
	@Value("classpath:Employees.graphqls")
	private  Resource schemaResource; 
	
	@Autowired
	EmployeeService empservice;
	@Autowired
	EmployeeRepository empRepositoryObj;
	private GraphQL graphQl;
	@PostConstruct
	public void loadSchema() throws IOException
	{
		File schemafile=schemaResource.getFile();
		TypeDefinitionRegistry resistory=new SchemaParser().parse(schemafile);
		RuntimeWiring wiring = buildWiring();
		GraphQLSchema grpschema=new SchemaGenerator().makeExecutableSchema(resistory, wiring);
		graphQl=GraphQL.newGraphQL(grpschema).build();
	}
	
	private RuntimeWiring buildWiring() {
		DataFetcher<List<Employees>> fetcher1=data ->{
			return  empservice.findAllEmps();
		};
		DataFetcher<Employees> fetcher12=data ->{
			return  empservice.findSingleEmp(data.getArgument("id"));
		};
		return RuntimeWiring.newRuntimeWiring().type("Query",typeWriting  ->
			typeWriting.dataFetcher("getAllEmp", fetcher1).dataFetcher("getSingleEmp", fetcher12)).build();
		
	}

	@PostMapping("/addEmp")
	public String addEmployees(@RequestBody Employees employees)
	{
		Employees emp = empservice.addEmp(employees);
		//empRepositoryObj.save(employees);
		return "emp";
	}
	
	@PostMapping("/AllEmps")
	public ResponseEntity<Object> AllEmps(@RequestBody String query)
	{
		ExecutionResult result=  graphQl.execute(query);
		return new ResponseEntity <Object>(result,HttpStatus.OK);
	}
	@PostMapping("/SingleEmps")
	public ResponseEntity<Object> SingleEmps(@RequestBody String query)
	{
		ExecutionResult result=  graphQl.execute(query);
		return new ResponseEntity <Object>(result,HttpStatus.OK);
	}
	
	

	
}
