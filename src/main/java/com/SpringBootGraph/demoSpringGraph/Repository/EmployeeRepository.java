package com.SpringBootGraph.demoSpringGraph.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.SpringBootGraph.demoSpringGraph.Pojo.Employees;
@Repository
public interface EmployeeRepository extends CrudRepository<Employees, Integer> {

	Employees findByid(Object argument);


}
