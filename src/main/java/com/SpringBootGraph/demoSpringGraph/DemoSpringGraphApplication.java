package com.SpringBootGraph.demoSpringGraph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringGraphApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringGraphApplication.class, args);
	}

}
