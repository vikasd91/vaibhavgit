package com.SpringBootGraph.demoSpringGraph.Pojo;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
@Table
  public class Employees {
  
	@PrimaryKey
	private @NotNull String id;
	private @NotNull double salary;
	private @NotNull String firstName;
	private @NotNull String lastName;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

  
  
  
  
  }
 
/*
 * @Table(name = "t4", keyspace = "test") public class Employees {
 * 
 * @PartitionKey int id;
 * 
 * @ClusteringColumn
 * 
 * @Column(name = "c") int clCol;
 * 
 * @Column(name = "t") String text; // ... getters/setters, etc. }
 */