package com.SpringBootGraph.demoSpringGraph.Service;
import java.util.List;

import com.SpringBootGraph.demoSpringGraph.Pojo.Employees;

public interface EmployeeService {

	Employees addEmp(Employees employees);

	List<Employees> findAllEmps();

	Employees findSingleEmp(Object argument);

	
}
